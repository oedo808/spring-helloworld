package com.example.demo;
import com.example.demo.XssModel;
import org.springframework.web.util.*;


import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner; // Import the Scanner class to read text files

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@RestController
public class HelloworldController {
	
	
	@Value("${spring.activemq.user}") String username;
	@Value("${spring.activemq.password}") String password;
	@RequestMapping("/id")
	public String index(@RequestParam String id) {
		//System.out.println();
		 String Password = "Pass1234";
			String filePath = "/Users/ayoung/Apps/Java/spring-helloworld/helloworld/";
			String fullPath = filePath + id;
			//String canonicalPath = org.springframework.util.StringUtils.cleanPath(fullPath);
			//Path testPath = new File(fullPath);
		String pattern = "/[a-zA-Z_0-9]+(/[a-zA-Z_0-9]+)*[.]txt$";
		String matches = "Matches Pattern:  " + id.matches(pattern);
		System.out.println(matches);
			System.out.println("cleanPath: " + org.springframework.util.StringUtils.cleanPath(fullPath));
			Path testPath = Paths.get(fullPath);
			String canonicalPath;
			try {
				canonicalPath = testPath.toRealPath().toString();
				System.out.println("CanPath: " + canonicalPath);
				if(canonicalPath.startsWith(filePath)) {
					File readFile = new File(filePath + id);
					Scanner myReader = new Scanner(readFile);
					while (myReader.hasNextLine()) {
						id = id + myReader.nextLine();
						
					}
					myReader.close();
					return id;
				}
				else {
					return canonicalPath;
				}
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return id + " " +matches;
			
			
			
			/*
				try {
					File readFile = new File(filePath + id);
					Scanner myReader = new Scanner(readFile);
					while (myReader.hasNextLine()) {
						id = id + myReader.nextLine();
						
					}
					myReader.close();
					return id;
				} catch (FileNotFoundException e) {
				      System.out.println("An error occurred.");
				      e.printStackTrace();
				      return id;
				    }*/
				
			
		}
	/*@RequestMapping("/callablestatement")
	public String callableStatement(@RequestParam String field) {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:/Users/ayoung/Apps/Java/spring-helloworld/helloworld/chinook.db");
			java.sql.CallableStatement myCall;
			String sql = "SELECT * from employees where FirstName=?";
			myCall = 
			
			
			
		}catch(SQLException e) {
			System.err.println(e.getMessage());
		}
		finally {
			try {
				if(connection != null)
					connection.close();
			}
			catch(SQLException e) {
				System.err.println(e.getMessage());
			}
		}
		return "test";
	}*/
		
	@RequestMapping("/db")
	public String db(@RequestParam String table) {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:/Users/ayoung/Apps/Java/spring-helloworld/helloworld/chinook.db");
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30);
			//String sql = "select * from " + table; //received SQLi vuln flaw ID 4
			String firstSQL = "select name from sqlite_master where type='table' and name not like 'sqlite_%';";
			ResultSet allTables = statement.executeQuery(firstSQL);
			List<String> tableNames = new ArrayList<String>();
			while (allTables.next()) {
				System.out.println(allTables.getString(1));
				tableNames.add(allTables.getString(1));
			}
			if(tableNames.contains(table))
			{
				System.out.println("Table is in list of SQL tables");
				int arrElement = tableNames.indexOf(table);
				String sql = "select * from " + tableNames.get(arrElement);
				StringBuilder str = new StringBuilder();
				ResultSet rs = statement.executeQuery(sql);
				while(rs.next()) {
					str.append(rs.getInt(1) + " | " + rs.getString(2) + " | " + rs.getInt(3));
					
				}
				System.out.println(str);
			}
			
			
		}
		catch(SQLException e) {
			System.err.println(e.getMessage());
		}
		finally {
			try {
				if(connection != null)
					connection.close();
			}
			catch(SQLException e) {
				System.err.println(e.getMessage());
			}
		}
		
		
		return "test";
	}
	@RequestMapping("/crypto")
	public String crypto(@RequestParam String cleartext) throws InvalidKeyException, InvalidAlgorithmParameterException {
		javax.crypto.Cipher badCipher;
		javax.crypto.Cipher goodCipher;
		java.security.KeyPairGenerator keyGen;
		java.security.SecureRandom secRandom = new java.security.SecureRandom();
		byte cipherKey[] = null;
		String returnText = "";
		try {
			badCipher = javax.crypto.Cipher.getInstance("AES");//CWE-327, default AES instance assumed to be ECB
			badCipher = javax.crypto.Cipher.getInstance("AES/ECB/PKCS5Padding");
			//returnText = badCipher.getAlgorithm();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			goodCipher = javax.crypto.Cipher.getInstance("AES/GCM/PKCS5Padding");
			goodCipher = javax.crypto.Cipher.getInstance("AES/GCM/NoPadding");
			byte[] iv = new byte[12];
			secRandom.nextBytes(iv);
			goodCipher.init(2, new javax.crypto.spec.SecretKeySpec(cipherKey, "AES"), new javax.crypto.spec.GCMParameterSpec(128, iv));
			//goodCipher = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
			//goodCipher = javax.crypto.Cipher.getInstance("RSA");
			//00394711 - flaw 25
			keyGen = java.security.KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(2048);
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			String salt = "replaceThisSalt"; //CWE-321 for hard-coded cryptographic key
			String password = "replaceThisPassword"; //CWE-259 for hard-coded password
			PBEKeySpec keyspec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 1000);
			SecretKeyFactory skf;
			try {
				//skf = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_128");
				skf = SecretKeyFactory.getInstance("PBEWithSHAAnd40BitRC2");
				SecretKey key = skf.generateSecret(keyspec);
				//Cipher cipher = Cipher.getInstance("PBEWithHmacSHA256AndAES_128");
				Cipher cipher = Cipher.getInstance("PBEWithSHAAnd40BitRC2");
				cipher = Cipher.getInstance("PBEWithSHAAnd3KeyTripleDES");
				cipher = Cipher.getInstance("PBEWithSHA1AndRC2");
				cipher = Cipher.getInstance("PBEWithSHAAnd128BitRC4");
				cipher = Cipher.getInstance("PBEWithMD2AndRC2");
				cipher = Cipher.getInstance("PBEWithMD5AndRC2");
				cipher = Cipher.getInstance("PBEWithSHAAnd40BitRC2");
				cipher = Cipher.getInstance("PBEWithSHAAnd40BitRC4");
				cipher = Cipher.getInstance("PBEWithSHAAnd3KeyTripleDESDeprecated");
				cipher = Cipher.getInstance("PBEWithSHA1AndTripleDES");
				cipher = Cipher.getInstance("PBEWithMD2AndDES");
				cipher = Cipher.getInstance("PBEWithMD2AndTripleDES");
				// end PBE's
				cipher = Cipher.getInstance("DES/CBC/NoPadding");
				cipher = Cipher.getInstance("TripleDES/CBC/NoPadding");
				cipher = Cipher.getInstance("RSAwithNoPad");
				cipher = Cipher.getInstance("ELGamal");
				cipher = Cipher.getInstance("ARCFOUR");
				cipher = Cipher.getInstance("AESWrap");
				cipher = Cipher.getInstance("RSA/ /OAEPPaddingSHA-256");
				cipher = Cipher.getInstance("RSA/ /OAEPPaddingSHA-512");
				cipher = Cipher.getInstance("RSA");
				cipher = Cipher.getInstance("RC2");
				cipher = Cipher.getInstance("RSAforSSL");
				cipher = Cipher.getInstance("RC4");
				cipher = Cipher.getInstance("Blowfish");
				cipher = Cipher.getInstance("DES");
				cipher = Cipher.getInstance("DESede");
				cipher = Cipher.getInstance("Mars");
				cipher = Cipher.getInstance("RSA/ /OAEPPaddingSHA-384");
				cipher = Cipher.getInstance("RSA/ /OAEPPaddingSHA-1");
				
				
				cipher.init(Cipher.ENCRYPT_MODE, key);
				byte[] ciphertext = cipher.doFinal(cleartext.getBytes());
				returnText = ciphertext.toString();
			} catch (NoSuchAlgorithmException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (InvalidKeySpecException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			
		
		return returnText;
	}
	@RequestMapping("/creds")
	public String creds() {
		Logger LOGGER = Logger.getLogger("myLogger");
		LOGGER.log(Level.INFO, returnId().toString());
		LOGGER.log(Level.INFO, this.toString());
		
		String testString = "<script>alert(1234)</script>";
		String testEncode = org.owasp.encoder.Encode.forHtmlContent(testString);
		char c[] = testEncode.toCharArray();
		String convertedToString = new String(c);
		//System.out.println(testEncode.toCharArray());
		System.out.println("encoded Length" + testEncode.length() + ":" + testEncode);
		System.out.println("charArrayLength" + c.length + ":" + c.toString());
		System.out.println("convertedString" + convertedToString.length() + ":" + convertedToString);
		
		/*for (int i=0; i<c.length; i++) {
			System.out.println(c[i]);
		}*/
		
		
		System.out.println(returnId().toString());
		System.out.println(this);
		
		return username + "\r\n" + password;
	}
	@RequestMapping("/servletPath")
	public String servletPath(HttpServletRequest req) {
		String servlPath = req.getServletPath();
		String pInfo = req.getPathInfo();
		return "servletPath: " + servlPath + " pathInfo: " + pInfo;
	}
	@RequestMapping("/xmlEE")
	public String xmlEE()
	{
		org.w3c.dom.Document doc;
		javax.xml.parsers.DocumentBuilder xdb;
		String xmlEEtest = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE foo [ <!ENTITY xxe SYSTEM \"file:///etc/passwd\"> ]><stockCheck><productId>&xxe;</productId></stockCheck>";
		org.apache.soap.util.xml.XMLParserUtils.refreshDocumentBuilderFactory("org.apache.crimson.jaxp.DocumentBuilderFactoryImpl", true, false);
		String safeXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><stockCheck><productId>test</productId></stockCheck>";
		//org.apache.crimson.jaxp.DocumentBuilderFactoryImpl dbfimpl = new org.apache.crimson.jaxp.DocumentBuilderFactoryImpl();
		javax.xml.parsers.DocumentBuilderFactory dbf = new org.apache.crimson.jaxp.DocumentBuilderFactoryImpl();
		try {
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setXIncludeAware(false);
			dbf.setExpandEntityReferences(false);
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		xdb = org.apache.soap.util.xml.XMLParserUtils.getXMLDocBuilder();
		try {
			doc = xdb.parse(new org.xml.sax.InputSource(safeXML));
			return doc.toString();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			return e.getMessage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		
		
	}
	public java.util.UUID returnId() {
		
		return java.util.UUID.randomUUID();
	}
	
	
	
}