package com.example.demo;

import org.springframework.web.util.HtmlUtils;

public class XssModel {
	private String taintXss;
	
	public void setTaintXss(String taintXss) {
		this.taintXss = taintXss;
	}
	public String getTaintXss() {
		//return this.taintXss;
		return HtmlUtils.htmlEscape(this.taintXss);
	}
}
