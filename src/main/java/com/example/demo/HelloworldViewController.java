package com.example.demo;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloworldViewController {
	@GetMapping("/")
	public ModelAndView jspView() {
		//model.addAttribute("taintXSS", new XssModel());
		//model.addAttribute("taintXSS", taintXSS.getTaintXss());
		//return "index";
		return new ModelAndView("index", "taintXss", new XssModel());
		
	}
	@PostMapping("/xssTest")
	public String setXssTest(@ModelAttribute("taintXss") XssModel taintXSS, BindingResult result, ModelMap model)
	{
		Logger LOGGER = Logger.getLogger("myLogger");
		if(result.hasErrors()) {
			LOGGER.log(Level.INFO, "error");
		}
		//model.addAttribute("taintXSS", taintXSS);
		model.addAttribute("taintXSS", taintXSS.getTaintXss());
		return "index";
		//xssModel.setTaintXss(taintXss);
		
	}
}
