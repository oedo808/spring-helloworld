<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <jsp:useBean id="hwcont" class="com.example.demo.HelloworldController" />
    <jsp:useBean id="xssModel" class="com.example.demo.XssModel" />
    <jsp:useBean id="hwview" class="com.example.demo.HelloworldViewController" />
    
 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	String xt = request.getParameter("xt");
%>
	<form action="/xssTest" method="post">
		<form:input type="text" path="taintXss" /> 
		<!-- <input id="taintXss" type="text" /> -->
		<input type="submit" value="Submit" />
	</form>
	<%= hwcont.returnId() %>
	<form action="taintXSS.jsp" method="post">
		<input type="text" />
		<input type="submit" value="submit" />
	</form>
	<jsp:setProperty property="taintXss" name="xssModel" value="<%=xt %>"/>
	
	<jsp:getProperty property="taintXss" name="xssModel"/>
</body>
</html>