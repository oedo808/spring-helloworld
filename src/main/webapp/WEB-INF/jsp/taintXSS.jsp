<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:useBean id="xssModel" class="com.example.demo.XssModel" />

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:setProperty property="*" name="xssModel"/>
	
	<jsp:getProperty property="taintXss" name="xssModel"/>
</body>
</html>